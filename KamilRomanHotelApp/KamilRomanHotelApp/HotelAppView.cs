﻿using KamilRomanHotelApp.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KamilRomanHotelApp
{
    public partial class HotelAppView : Form
    {
        ReservationsRepository reservationsRepository;
        public HotelAppView()
        {
            InitializeComponent();
            reservationsRepository = new ReservationsRepository();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            refreshDataGridView();
            dataGridViewColumnsResize();
        }

        private void dgvReservations_SelectionChanged(object sender, EventArgs e)
        {
            this.btModifyReservation.Visible = true;
            this.btRemoveReservation.Visible = true;
        }

        private void dataGridViewColumnsResize()
        {
             for (int i = 0; i < dgvReservations.Columns.Count - 1; i++)
            {
                dgvReservations.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            }
            dgvReservations.Columns[dgvReservations.Columns.Count - 1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            for (int i = 0; i < dgvReservations.Columns.Count; i++)
            {
                int colw = dgvReservations.Columns[i].Width;
                dgvReservations.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                dgvReservations.Columns[i].Width = colw;
            }
        }

        private void btAddReservation_Click(object sender, EventArgs e)
        {
            ReservationForm reservationForm = new ReservationForm(this);
            reservationForm.Show();
        }

        private void btRemoveReservation_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Czy na pewno chcesz usunąć tą rezerwację?", "Uwaga!", MessageBoxButtons.YesNo) == DialogResult.Yes) {
                if (reservationsRepository.RemoveReservation(this.dgvReservations.Rows[dgvReservations.CurrentCell.RowIndex]))
                {
                    MessageBox.Show("Rezerwacja usunięta pomyślnie");
                    refreshDataGridView();
                }
                else
                    MessageBox.Show("Wystąpił problem przy usunięciu rezerwacji!");
            }
        }

        public void refreshDataGridView()
        {
            this.dgvReservations.DataSource = reservationsRepository.GetReservations(this.dateTimePickerFrom.Value, dateTimePickerTo.Value);
            this.dgvReservations.Columns["Customer"].Visible = false;
            this.dgvReservations.Columns["Room"].Visible = false;
        }

        private void btModifyReservation_Click(object sender, EventArgs e)
        {
            ReservationForm reservationForm = new ReservationForm(this, this.dgvReservations.Rows[dgvReservations.CurrentCell.RowIndex], false);
            reservationForm.Show();
        }
    }
}
