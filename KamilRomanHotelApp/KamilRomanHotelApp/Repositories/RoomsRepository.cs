﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KamilRomanHotelApp.Repositories
{
    class RoomsRepository
    {
        private HotelAppContext context;
        public string[] loadRooms()
        {
            using (context = new HotelAppContext())
            {
                var rooms = context.Rooms.Select(r => new { r.Number }).ToArray();
                List<string> roomsArray = new List<string>();
                foreach (var r in rooms)
                {
                    roomsArray.Add(r.Number.ToString());
                }
                return roomsArray.ToArray();
            }
        }

        internal Room findRoomByNumber(int roomNumber)
        {
            using(context = new HotelAppContext())
            {
                return context.Rooms.Where(r => r.Number == roomNumber).FirstOrDefault();
            }
        }

        internal Room findById(int id)
        {
            using(context = new HotelAppContext())
            {
                return context.Rooms.Where(r => r.RoomId == id).FirstOrDefault();
            }
        }

        public BindingSource GetRooms(DateTime time)
        {
            using (context = new HotelAppContext())
            {
                BindingSource source = new BindingSource();
                source.DataSource = context.Rooms.SqlQuery(
                    @"CREATE VIEW [Pokoje] AS 
                    SELECT Rooms.Number, Rooms.Floor, 
                    Count(SELECT * FROM Reservations WHERE Reservations.Room_id = Rooms.RoomId && Reservations.From > @dateTime) AS Future_reservations_count, 
                    IF(SELECT COUNT(SELECT * FROM Reservations WHERE Reservations.Room_id = Rooms.RoomId AND Reservations.From <= @dateTime AND Reservations.To >= @dateTime) > 0, 
                        SELECT Customers.Name, Customers.Surname, Customers.Email, Customers.Phone_number FROM Customers JOIN Reservations ON Reservations.Customer_id = Customers.CustomerId 
                        WHERE Reservations.From <= @dateTime AND Reservations.To >= @dateTime AND Reservations.Room_id = Rooms.RoomId, 
                    "")
                    FROM Rooms
                    JOIN Reservations ON Rooms.RoomId = Reservations.Room_id
                    JOIN Customers ON Reservations.Customer_id = Customers.CustomerId
                    ORDER BY Future_reservations_count DESC"
                );
                return source;
            }
        }
    }
}
