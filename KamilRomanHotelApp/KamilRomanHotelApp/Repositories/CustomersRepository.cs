﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KamilRomanHotelApp.Repositories
{
    class CustomersRepository
    {
        private HotelAppContext context;
        public string[] loadCustomers()
        {
            List<string> customerArray;
            using (context = new HotelAppContext())
            {
                var customers = context.Customers.Select(c => new { c.Name, c.Surname }).ToArray();
                customerArray = new List<string>();
                foreach(var c in customers)
                {
                    customerArray.Add(c.Name + " " + c.Surname);
                }
            }
            customerArray.Add("Nowy");
            return customerArray.ToArray();
        }

        internal Customer findCustomerByName(string customerName)
        {
            using(context = new HotelAppContext())
            {
                var cust = customerName.Split(' ');
                var name = cust[0];
                var surname = cust[1];
                return context.Customers.Where(c => c.Name == name && c.Surname == surname).FirstOrDefault();
            }
        }

        internal bool checkIfCustomerExists(Customer customer)
        {
            using (context = new HotelAppContext())
            {
                if (context.Customers.Where(c => c.Name == customer.Name && c.Surname == customer.Surname && c.Email == customer.Email && c.Phone_number == customer.Phone_number).Count() > 0)
                    return true;
                return false;
            }
        }

        internal Customer findCustomerById(int id)
        {
            using (context = new HotelAppContext())
            {
                return context.Customers.Where(c => c.CustomerId == id).FirstOrDefault();
            }
        }

        public BindingSource GetCustomers(DateTime time)
        {
            using (context = new HotelAppContext())
            {
                BindingSource source = new BindingSource();
                source.DataSource = context.Customers.SqlQuery(
                    @"CREATE VIEW [Klienci] AS
                    SELECT Customers.Name, Customers.Surname, Customers.Email, Customers.Phone_number, 
                    COUNT(SELECT * FROM Reservations WHERE Reservations.Customer_id = Customers.CustomerId) AS Number_of_reservations, 
                    AVG(SELECT DATEDIFF(day, Reservations.To, Reservations.From) AS DateFiff FROM Reservations WHERE Reservations.Customer_id = Customers.CustomerId) AS Avg_time, 
                    FIRST(SELECT Rooms.Number FROM (SELECT COUNT (Reservations.ReservationId) AS Ilosc, Rooms.Number 
                        FROM Reservations JOIN Rooms.RoomId = Reservations.Room_id WHERE Reservations.Customer_id = Customers.CustomerId 
                        GROUP BY Room_id 
                        ORDER BY Ilosc DESC))) 
                    FROM Customers
                    JOIN Reservations ON Customers.CustomerId = Reservations.Customer_id 
                    JOIN Rooms ON Reservations.Room_id = Rooms.RoomId
                    ORDER BY Number_of_reservations DESC"
                    );
                return source;
            }
        }
    }
}
