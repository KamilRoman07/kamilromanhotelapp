﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KamilRomanHotelApp.Repositories
{
    class ReservationsRepository
    {
        private HotelAppContext context;
        private CustomersRepository customerRepository;
        private RoomsRepository roomsRepository;
        public ReservationsRepository()
        {
            customerRepository = new CustomersRepository();
            roomsRepository = new RoomsRepository();

        }

        //Get all reservations
        public BindingSource GetReservations(DateTime from, DateTime to)
        {
            using (context = new HotelAppContext())
            {
                BindingSource source = new BindingSource();
                source.DataSource = context.Reservations.Include("Customer").Include("Room").Where(reservation => reservation.From <= to && reservation.To >= from).ToList<Reservation>();
                return source;
            }
        }

        //Get reservation by id
        private Reservation GetReservation(int reservationId)
        {
            using (context = new HotelAppContext())
            {
                return context.Reservations.Where(r => r.ReservationId == reservationId).FirstOrDefault();
            }
        }

        //Check reservation in update
        internal bool CheckReservation(int roomNumber, DateTime from, DateTime to, int resId)
        {
            Room room = roomsRepository.findRoomByNumber(roomNumber);
            using (context = new HotelAppContext())
            {
                if (context.Reservations.Where(r => r.Room_id == room.RoomId && r.From <= to && r.To >= from && r.ReservationId != resId).ToArray().Length > 0)
                    return true;
            }
            return false;
        }

        //Check reservation in save
        public bool CheckReservation(int roomNumber, DateTime from, DateTime to)
        {
            Room room = roomsRepository.findRoomByNumber(roomNumber);
            using (context = new HotelAppContext())
            {
                if (context.Reservations.Where(r => r.Room_id == room.RoomId && r.From <= to && r.To >= from).ToArray().Length > 0)
                    return true;
            }
            return false;
        }

        //Add reservation existing customer
        internal bool AddReservation(string customerName, string roomNumber, DateTime from, DateTime to, decimal guestAmount, decimal price)
        {
            using (context = new HotelAppContext())
            {
                Customer customer = customerRepository.findCustomerByName(customerName);
                Room room = roomsRepository.findRoomByNumber(Int32.Parse(roomNumber));
                var reservation = new Reservation()
                {
                    Customer_id = customer.CustomerId,
                    From = from,
                    To = to,
                    Guest_amount = Int32.Parse(guestAmount.ToString()),
                    Price_per_night = Int32.Parse(price.ToString()),
                    Room_id = room.RoomId
                };
                context.Reservations.Add(reservation);
                context.SaveChanges();
                return true;
            }
        }

        //Add reservation new customer
        internal bool AddReservation(string roomNumber, DateTime from, DateTime to, Customer customer, decimal guestAmount, decimal price)
        {
            using (context = new HotelAppContext())
            {
                if (customerRepository.checkIfCustomerExists(customer))
                {
                    MessageBox.Show("Klient już ustnieje");
                    return false;
                }
                else
                {
                    context.Customers.Add(customer);
                    context.SaveChanges();
                    customer = customerRepository.findCustomerByName(customer.Name + " " + customer.Surname);
                    Room room = roomsRepository.findRoomByNumber(Int32.Parse(roomNumber));
                    var reservation = new Reservation()
                    {
                        Customer_id = customer.CustomerId,
                        From = from,
                        To = to,
                        Guest_amount = Int32.Parse(guestAmount.ToString()),
                        Price_per_night = Int32.Parse(price.ToString()),
                        Room_id = room.RoomId,
                    };
                    context.Reservations.Add(reservation);
                    context.SaveChanges();
                    return true;
                }
            }
        }

        //Delete reservation
        internal bool RemoveReservation(DataGridViewRow dataGridViewRow)
        {
            using (context = new HotelAppContext())
            {
                int reservationId = Int32.Parse(dataGridViewRow.Cells[0].Value.ToString());
                bool oldValidateOnSaveEnabled = context.Configuration.ValidateOnSaveEnabled;

                try
                {
                    context.Configuration.ValidateOnSaveEnabled = false;

                    Reservation reservation = new Reservation { ReservationId = reservationId };

                    context.Reservations.Attach(reservation);
                    context.Entry(reservation).State = EntityState.Deleted;
                    context.SaveChanges();
                }
                finally
                {
                    context.Configuration.ValidateOnSaveEnabled = oldValidateOnSaveEnabled;
                }
                return true;
            }
        }

        //Update with new customer
        internal bool UpdateReservation(int resId, string roomNumber, DateTime from, DateTime to, Customer customer, decimal guestAmount, decimal price)
        {
            using(context = new HotelAppContext())
            {
                if (customerRepository.checkIfCustomerExists(customer))
                {
                    MessageBox.Show("Klient już ustnieje");
                    return false;
                }
                else
                {
                    Reservation reservation = GetReservation(resId);
                    if (reservation != null)
                    {
                        context.Customers.Add(customer);
                        context.SaveChanges();
                        customer = customerRepository.findCustomerByName(customer.Name + " " + customer.Surname);
                        Room room = roomsRepository.findRoomByNumber(Int32.Parse(roomNumber));
                        reservation.Customer_id = customer.CustomerId;
                        reservation.From = from;
                        reservation.To = to;
                        reservation.Room_id = room.RoomId;
                        reservation.Price_per_night = Int32.Parse(price.ToString());
                        reservation.Guest_amount = Int32.Parse(guestAmount.ToString());
                        context.Reservations.AddOrUpdate(reservation);
                        context.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }

        //Update without new customer
        internal bool UpdateReservation(int resId, string customerName, string roomNumber, DateTime from, DateTime to, decimal guestAmount, decimal price)
        {
            using (context = new HotelAppContext())
            {
                Reservation reservation = GetReservation(resId);
                Customer customer = customerRepository.findCustomerByName(customerName);
                Room room = roomsRepository.findRoomByNumber(Int32.Parse(roomNumber));
                using (context = new HotelAppContext())
                {
                    reservation.From = from;
                    reservation.To = to;
                    reservation.Customer_id = customer.CustomerId;
                    reservation.Guest_amount = Int32.Parse(guestAmount.ToString());
                    reservation.Price_per_night = Int32.Parse(price.ToString());
                    reservation.Room_id = room.RoomId;
                    context.Reservations.AddOrUpdate(reservation);
                    context.SaveChanges();
                }
                return true;
            }
        }
    }
}
