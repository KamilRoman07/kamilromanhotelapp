﻿using System;

namespace KamilRomanHotelApp
{
    partial class HotelAppView
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControlMain = new System.Windows.Forms.TabControl();
            this.reservatonsTab = new System.Windows.Forms.TabPage();
            this.btRemoveReservation = new System.Windows.Forms.Button();
            this.btModifyReservation = new System.Windows.Forms.Button();
            this.dateTimePickerTo = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerFrom = new System.Windows.Forms.DateTimePicker();
            this.dgvReservations = new System.Windows.Forms.DataGridView();
            this.btAddReservation = new System.Windows.Forms.Button();
            this.btSearchReservation = new System.Windows.Forms.Button();
            this.tabCustomers = new System.Windows.Forms.TabPage();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dgvPokoje = new System.Windows.Forms.DataGridView();
            this.dgvKlienci = new System.Windows.Forms.DataGridView();
            this.tabControlMain.SuspendLayout();
            this.reservatonsTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReservations)).BeginInit();
            this.tabCustomers.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPokoje)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKlienci)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControlMain
            // 
            this.tabControlMain.Controls.Add(this.reservatonsTab);
            this.tabControlMain.Controls.Add(this.tabCustomers);
            this.tabControlMain.Controls.Add(this.tabPage1);
            this.tabControlMain.Location = new System.Drawing.Point(-2, 1);
            this.tabControlMain.Name = "tabControlMain";
            this.tabControlMain.SelectedIndex = 0;
            this.tabControlMain.Size = new System.Drawing.Size(805, 450);
            this.tabControlMain.TabIndex = 2;
            // 
            // reservatonsTab
            // 
            this.reservatonsTab.Controls.Add(this.btRemoveReservation);
            this.reservatonsTab.Controls.Add(this.btModifyReservation);
            this.reservatonsTab.Controls.Add(this.dateTimePickerTo);
            this.reservatonsTab.Controls.Add(this.dateTimePickerFrom);
            this.reservatonsTab.Controls.Add(this.dgvReservations);
            this.reservatonsTab.Controls.Add(this.btAddReservation);
            this.reservatonsTab.Controls.Add(this.btSearchReservation);
            this.reservatonsTab.Location = new System.Drawing.Point(4, 22);
            this.reservatonsTab.Name = "reservatonsTab";
            this.reservatonsTab.Padding = new System.Windows.Forms.Padding(3);
            this.reservatonsTab.Size = new System.Drawing.Size(797, 424);
            this.reservatonsTab.TabIndex = 0;
            this.reservatonsTab.Text = "Rezerwacje";
            this.reservatonsTab.UseVisualStyleBackColor = true;
            // 
            // btRemoveReservation
            // 
            this.btRemoveReservation.Location = new System.Drawing.Point(346, 50);
            this.btRemoveReservation.Name = "btRemoveReservation";
            this.btRemoveReservation.Size = new System.Drawing.Size(100, 23);
            this.btRemoveReservation.TabIndex = 8;
            this.btRemoveReservation.Text = "Usuń rezerwację";
            this.btRemoveReservation.UseVisualStyleBackColor = true;
            this.btRemoveReservation.Visible = false;
            this.btRemoveReservation.Click += new System.EventHandler(this.btRemoveReservation_Click);
            // 
            // btModifyReservation
            // 
            this.btModifyReservation.Location = new System.Drawing.Point(221, 50);
            this.btModifyReservation.Name = "btModifyReservation";
            this.btModifyReservation.Size = new System.Drawing.Size(119, 23);
            this.btModifyReservation.TabIndex = 7;
            this.btModifyReservation.Text = "Modyfikuj rezerwację";
            this.btModifyReservation.UseVisualStyleBackColor = true;
            this.btModifyReservation.Visible = false;
            this.btModifyReservation.Click += new System.EventHandler(this.btModifyReservation_Click);
            // 
            // dateTimePickerTo
            // 
            this.dateTimePickerTo.Location = new System.Drawing.Point(221, 24);
            this.dateTimePickerTo.Name = "dateTimePickerTo";
            this.dateTimePickerTo.Size = new System.Drawing.Size(225, 20);
            this.dateTimePickerTo.TabIndex = 6;
            // 
            // dateTimePickerFrom
            // 
            this.dateTimePickerFrom.Location = new System.Drawing.Point(3, 24);
            this.dateTimePickerFrom.Name = "dateTimePickerFrom";
            this.dateTimePickerFrom.Size = new System.Drawing.Size(212, 20);
            this.dateTimePickerFrom.TabIndex = 5;
            // 
            // dgvReservations
            // 
            this.dgvReservations.AllowUserToAddRows = false;
            this.dgvReservations.AllowUserToDeleteRows = false;
            this.dgvReservations.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvReservations.Location = new System.Drawing.Point(0, 156);
            this.dgvReservations.MultiSelect = false;
            this.dgvReservations.Name = "dgvReservations";
            this.dgvReservations.Size = new System.Drawing.Size(801, 268);
            this.dgvReservations.TabIndex = 4;
            this.dgvReservations.SelectionChanged += new System.EventHandler(this.dgvReservations_SelectionChanged);
            // 
            // btAddReservation
            // 
            this.btAddReservation.Location = new System.Drawing.Point(117, 50);
            this.btAddReservation.Name = "btAddReservation";
            this.btAddReservation.Size = new System.Drawing.Size(98, 23);
            this.btAddReservation.TabIndex = 3;
            this.btAddReservation.Text = "Dodaj rezerwację";
            this.btAddReservation.UseVisualStyleBackColor = true;
            this.btAddReservation.Click += new System.EventHandler(this.btAddReservation_Click);
            // 
            // btSearchReservation
            // 
            this.btSearchReservation.Location = new System.Drawing.Point(3, 50);
            this.btSearchReservation.Name = "btSearchReservation";
            this.btSearchReservation.Size = new System.Drawing.Size(108, 23);
            this.btSearchReservation.TabIndex = 2;
            this.btSearchReservation.Text = "Szukaj rezerwacji";
            this.btSearchReservation.UseVisualStyleBackColor = true;
            this.btSearchReservation.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // tabCustomers
            // 
            this.tabCustomers.Controls.Add(this.dgvKlienci);
            this.tabCustomers.Location = new System.Drawing.Point(4, 22);
            this.tabCustomers.Name = "tabCustomers";
            this.tabCustomers.Padding = new System.Windows.Forms.Padding(3);
            this.tabCustomers.Size = new System.Drawing.Size(797, 424);
            this.tabCustomers.TabIndex = 1;
            this.tabCustomers.Text = "Klienci";
            this.tabCustomers.UseVisualStyleBackColor = true;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dgvPokoje);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(797, 424);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Pokoje";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dgvPokoje
            // 
            this.dgvPokoje.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPokoje.Location = new System.Drawing.Point(3, 3);
            this.dgvPokoje.Name = "dgvPokoje";
            this.dgvPokoje.Size = new System.Drawing.Size(791, 421);
            this.dgvPokoje.TabIndex = 0;
            // 
            // dgvKlienci
            // 
            this.dgvKlienci.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvKlienci.Location = new System.Drawing.Point(3, 3);
            this.dgvKlienci.Name = "dgvKlienci";
            this.dgvKlienci.Size = new System.Drawing.Size(791, 421);
            this.dgvKlienci.TabIndex = 0;
            // 
            // HotelAppView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tabControlMain);
            this.Name = "HotelAppView";
            this.Text = "Hotel App";
            this.tabControlMain.ResumeLayout(false);
            this.reservatonsTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvReservations)).EndInit();
            this.tabCustomers.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPokoje)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKlienci)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion

        private System.Windows.Forms.TabControl tabControlMain;
        private System.Windows.Forms.TabPage reservatonsTab;
        private System.Windows.Forms.Button btAddReservation;
        private System.Windows.Forms.Button btSearchReservation;
        private System.Windows.Forms.TabPage tabCustomers;
        private System.Windows.Forms.Button btRemoveReservation;
        private System.Windows.Forms.Button btModifyReservation;
        private System.Windows.Forms.DateTimePicker dateTimePickerTo;
        private System.Windows.Forms.DateTimePicker dateTimePickerFrom;
        private System.Windows.Forms.DataGridView dgvReservations;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dgvPokoje;
        private System.Windows.Forms.DataGridView dgvKlienci;
    }
}

