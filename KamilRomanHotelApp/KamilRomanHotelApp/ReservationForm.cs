﻿using KamilRomanHotelApp.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KamilRomanHotelApp
{
    public partial class ReservationForm : Form
    {
        HotelAppView hotelView;
        RoomsRepository roomsRepository;
        CustomersRepository customersRepository;
        ReservationsRepository reservationRepository;
        DataGridViewRow reservationInfo;
        bool save = true;
        public ReservationForm()
        {
            Initialize();
        }

        public ReservationForm(HotelAppView hotelAppView)
        {
            Initialize();
            hotelView = hotelAppView;
        }

        public ReservationForm(HotelAppView hotelAppView, DataGridViewRow dataGridViewRow, bool saveMode)
        {
            Initialize();
            hotelView = hotelAppView;
            save = saveMode;
            reservationInfo = dataGridViewRow;        
        }

        private void ReservationForm_Shown(object sender, EventArgs e)
        {
            Customer customer = customersRepository.findCustomerById(Int32.Parse(reservationInfo.Cells[2].Value.ToString()));
            cbCustomers.SelectedIndex = cbCustomers.Items.IndexOf(customer.Name + " " + customer.Surname);
            Room room = roomsRepository.findById(Int32.Parse(reservationInfo.Cells[1].Value.ToString()));
            cbRooms.SelectedIndex = cbRooms.Items.IndexOf(room.Number.ToString());
            this.dateTimePickerFrom.Value = Convert.ToDateTime(reservationInfo.Cells[3].Value.ToString());
            this.dateTimePickerTo.Value = Convert.ToDateTime(reservationInfo.Cells[4].Value.ToString());
            this.nudPrice.Value = Convert.ToDecimal(reservationInfo.Cells[5].Value.ToString());
            this.nudGuestAmount.Value = Convert.ToDecimal(reservationInfo.Cells[6].Value.ToString());
        }

        private void Initialize()
        {
            InitializeComponent();
            roomsRepository = new RoomsRepository();
            customersRepository = new CustomersRepository();
            reservationRepository = new ReservationsRepository();
            LoadCustomers();
            LoadRooms();
        }

        private void LoadRooms()
        {
            this.cbRooms.DataSource = roomsRepository.loadRooms();
        }

        private void LoadCustomers()
        {
            this.cbCustomers.DataSource = customersRepository.loadCustomers();
        }

        private void cbCustomers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cbCustomers.SelectedIndex == this.cbCustomers.Items.Count - 1)
            {
                this.tbName.Visible = true;
                this.tbSurname.Visible = true;
                this.tbEmail.Visible = true;
                this.tbPhone.Visible = true;
                this.label1.Visible = true;
                this.label2.Visible = true;
                this.label3.Visible = true;
                this.label4.Visible = true;
            }
            else
            {
                this.tbName.Visible = false;
                this.tbSurname.Visible = false;
                this.tbEmail.Visible = false;
                this.tbPhone.Visible = false;
                this.label1.Visible = false;
                this.label2.Visible = false;
                this.label3.Visible = false;
                this.label4.Visible = false;
            }
        }

        private void btSave_Click(object sender, EventArgs e)
        {
            //Check if reservation exists save
            if (reservationRepository.CheckReservation(Int32.Parse(this.cbRooms.SelectedValue.ToString()), this.dateTimePickerFrom.Value, this.dateTimePickerTo.Value) && save)
                MessageBox.Show("Termin dla wybranego pokoju jest już objęty rezerwacją. Proszę wybrać inny termin, lub inny pokój");
            //Check if reservation exists update
            else if (reservationRepository.CheckReservation(Int32.Parse(this.cbRooms.SelectedValue.ToString()), this.dateTimePickerFrom.Value, this.dateTimePickerTo.Value,
                Int32.Parse(reservationInfo.Cells[0].Value.ToString())) && !save)
                MessageBox.Show("Termin dla wybranego pokoju jest już objęty rezerwacją. Proszę wybrać inny termin, lub inny pokój");
            else
            {
                if (Zapisz())
                {
                    MessageBox.Show("Rezerwacja zapisana");
                    hotelView.refreshDataGridView();
                    this.Dispose();
                }
                else
                    MessageBox.Show("Wystąpił problem z zapisaniem rezerwacji");
            }
        }

        private bool Zapisz()
        {
            if (cbCustomers.SelectedIndex == cbCustomers.Items.Count - 1)
            {
                Customer customer = new Customer() {
                    Email = this.tbEmail.Text,
                    Name = this.tbName.Text,
                    Surname = this.tbSurname.Text,
                    Phone_number = this.tbPhone.Text
                };

                if (save)
                    //Add reservation new customer
                    return reservationRepository.AddReservation(cbRooms.SelectedValue.ToString(), this.dateTimePickerFrom.Value, this.dateTimePickerTo.Value, customer,
                        nudGuestAmount.Value, nudPrice.Value);
                else
                    //Update reservation new customer
                    return reservationRepository.UpdateReservation(Int32.Parse(reservationInfo.Cells[0].Value.ToString()), cbRooms.SelectedValue.ToString(), 
                        this.dateTimePickerFrom.Value, this.dateTimePickerTo.Value, customer, nudGuestAmount.Value, nudPrice.Value);
            }
            if(save)
                //Add reservation existing cusomer
                return reservationRepository.AddReservation(cbCustomers.SelectedValue.ToString(), cbRooms.SelectedValue.ToString(), this.dateTimePickerFrom.Value, 
                    this.dateTimePickerTo.Value, nudGuestAmount.Value, nudPrice.Value);
            else
                //Update reservation existing customer
                return reservationRepository.UpdateReservation(Int32.Parse(reservationInfo.Cells[0].Value.ToString()), cbCustomers.SelectedValue.ToString(), 
                    cbRooms.SelectedValue.ToString(), this.dateTimePickerFrom.Value, this.dateTimePickerTo.Value, nudGuestAmount.Value, nudPrice.Value);
        }
    }
}
